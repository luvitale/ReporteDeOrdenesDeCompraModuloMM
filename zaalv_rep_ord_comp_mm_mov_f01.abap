*&---------------------------------------------------------------------*
*&  Include           ZAALV_REP_ORD_COMP_MM_MOV_F01
*&---------------------------------------------------------------------*

INCLUDE:
  zaalv_rep_ord_comp_mm_mov_f02.

*&---------------------------------------------------------------------*
*&      Form  GET_MOVEMENTS_DATA
*&---------------------------------------------------------------------*
FORM get_movements_data
  USING
    us_t_alv     TYPE tyt_alv
  CHANGING
    ch_t_alv_movements TYPE tyt_alv_movements.

  DATA:
    lt_alv  TYPE tyt_alv,
    le_alv  TYPE ty_alv,
    lt_mseg TYPE tyt_mseg,
    lt_mkpf TYPE tyt_mkpf.

  LOOP AT t_alv INTO le_alv
    WHERE sel EQ abap_true.

    APPEND le_alv TO lt_alv.

  ENDLOOP.

  PERFORM get_movements_data_db
    USING
      lt_alv[]
    CHANGING
      lt_mseg[]
      lt_mkpf[].

  PERFORM process_movements_data
    USING
      lt_alv[]
      lt_mseg[]
      lt_mkpf[]
    CHANGING
      ch_t_alv_movements[].

ENDFORM.                    " GET_MOVEMENTS_DATA

*&---------------------------------------------------------------------*
*&      Form  GET_MOVEMENTS_DATA_DB
*&---------------------------------------------------------------------*
FORM get_movements_data_db
  USING
    us_t_alv  TYPE tyt_alv
  CHANGING
    ch_t_mseg TYPE tyt_mseg
    ch_t_mkpf TYPE tyt_mkpf.

  IF us_t_alv[] IS NOT INITIAL.

    SELECT mblnr mjahr zeile ebeln ebelp bwart menge meins dmbtr waers
      FROM mseg
      INTO TABLE ch_t_mseg
      FOR ALL ENTRIES IN us_t_alv
    WHERE ebeln EQ us_t_alv-ebeln
      AND ebelp EQ us_t_alv-ebelp.

    IF ch_t_mseg[] IS NOT INITIAL.

      SELECT mblnr mjahr blart bldat budat
        FROM mkpf
        INTO TABLE ch_t_mkpf
        FOR ALL ENTRIES IN ch_t_mseg
      WHERE mblnr EQ ch_t_mseg-mblnr
        AND mjahr EQ ch_t_mseg-mjahr.

    ENDIF.

  ENDIF.

ENDFORM.                    " GET_MOVEMENTS_DATA_DB

*&---------------------------------------------------------------------*
*&      Form  PROCESS_MOVEMENTS_DATA
*&---------------------------------------------------------------------*
FORM process_movements_data
  USING
    us_t_alv   TYPE tyt_alv
    us_t_mseg  TYPE tyt_mseg
    us_t_mkpf  TYPE tyt_mkpf
  CHANGING
    ch_t_alv_movements TYPE tyt_alv_movements.

  FIELD-SYMBOLS:
    <lfs_alv>  TYPE ty_alv,
    <lfs_mseg> TYPE ty_mseg,
    <lfs_mkpf> TYPE ty_mkpf,
    <lfs_alv_movements> TYPE ty_alv_movements.

  LOOP AT us_t_alv ASSIGNING <lfs_alv>.

    LOOP AT us_t_mseg ASSIGNING <lfs_mseg>
      WHERE ebeln EQ <lfs_alv>-ebeln
        AND ebelp EQ <lfs_alv>-ebelp.

      READ TABLE us_t_mkpf ASSIGNING <lfs_mkpf>
        WITH KEY mblnr = <lfs_mseg>-mblnr
                 mjahr = <lfs_mseg>-mjahr.

      APPEND INITIAL LINE TO ch_t_alv_movements ASSIGNING <lfs_alv_movements>.

      IF <lfs_alv_movements> IS ASSIGNED.

        IF <lfs_alv> IS ASSIGNED.

          <lfs_alv_movements>-ebeln = <lfs_alv>-ebeln.
          <lfs_alv_movements>-ebelp = <lfs_alv>-ebelp.

          IF <lfs_mseg> IS ASSIGNED.

            <lfs_alv_movements>-mblnr = <lfs_mseg>-mblnr.
            <lfs_alv_movements>-mjahr = <lfs_mseg>-mjahr.
            <lfs_alv_movements>-zeile = <lfs_mseg>-zeile.
            <lfs_alv_movements>-bwart = <lfs_mseg>-bwart.
            <lfs_alv_movements>-menge = <lfs_mseg>-menge.
            <lfs_alv_movements>-meins = <lfs_mseg>-meins.
            <lfs_alv_movements>-dmbtr = <lfs_mseg>-dmbtr.
            <lfs_alv_movements>-waers = <lfs_mseg>-waers.

            IF <lfs_mkpf> IS ASSIGNED.

              <lfs_alv_movements>-blart = <lfs_mkpf>-blart.
              <lfs_alv_movements>-bldat = <lfs_mkpf>-bldat.
              <lfs_alv_movements>-budat = <lfs_mkpf>-budat.

            ENDIF.

          ENDIF.

        ENDIF.

      ENDIF.

    ENDLOOP.

  ENDLOOP.

ENDFORM.                    " PROCESS_MOVEMENTS_DATA

*&---------------------------------------------------------------------*
*&      Form  SHOW_MOVEMENTS_REPORT
*&---------------------------------------------------------------------*
FORM show_movements_report
  USING
    us_t_alv_movements TYPE tyt_alv_movements.

  DATA:
    lv_callback_program TYPE sy-repid,
    le_layout           TYPE slis_layout_alv,
    lt_fieldcat         TYPE slis_t_fieldcat_alv.

  lv_callback_program = sy-repid.

  PERFORM set_movements_layout
    CHANGING
      le_layout.

  PERFORM set_movements_fieldcat
    CHANGING
      lt_fieldcat[].

  SORT us_t_alv_movements.

  CALL FUNCTION 'REUSE_ALV_GRID_DISPLAY'
    EXPORTING
      i_callback_program = lv_callback_program
      is_layout          = le_layout
      it_fieldcat        = lt_fieldcat
    TABLES
      t_outtab           = us_t_alv_movements
    EXCEPTIONS
      program_error      = 1
      OTHERS             = 2.

  IF sy-subrc <> 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
            WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.

ENDFORM.                    " SHOW_MOVEMENTS_REPORT