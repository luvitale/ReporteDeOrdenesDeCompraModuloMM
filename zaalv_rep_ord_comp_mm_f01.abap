*&---------------------------------------------------------------------*
*&  Include           ZAALV_REP_ORD_COMP_MM_F01
*&---------------------------------------------------------------------*

INCLUDE:
  zaalv_rep_ord_comp_mm_f02.

*&---------------------------------------------------------------------*
*&      Form  GET_DATA
*&---------------------------------------------------------------------*
FORM get_data
  CHANGING ch_t_alv TYPE tyt_alv.

  DATA:
   lt_ekko  TYPE tyt_ekko,
   lt_ekpo  TYPE tyt_ekpo,
   lt_t161t TYPE tyt_t161t,
   lt_lfa1  TYPE tyt_lfa1,
   lt_makt  TYPE tyt_makt,
   lt_konv  TYPE tyt_konv,
   lt_eket  TYPE tyt_eket.

  PERFORM get_data_db
    CHANGING
      lt_ekko[]
      lt_t161t[]
      lt_lfa1[]
      lt_ekpo[]
      lt_eket[]
      lt_makt[]
      lt_konv[].

  PERFORM process_data
    USING
      lt_ekko[]
      lt_t161t[]
      lt_lfa1[]
      lt_ekpo[]
      lt_eket[]
      lt_makt[]
      lt_konv[]
    CHANGING
      ch_t_alv[].

ENDFORM.                    " GET_DATA

*&---------------------------------------------------------------------*
*&      Form  GET_DATA_DB
*&---------------------------------------------------------------------*
FORM get_data_db
  CHANGING
    ch_t_ekko  TYPE tyt_ekko
    ch_t_t161t TYPE tyt_t161t
    ch_t_lfa1  TYPE tyt_lfa1
    ch_t_ekpo  TYPE tyt_ekpo
    ch_t_eket  TYPE tyt_eket
    ch_t_makt  TYPE tyt_makt
    ch_t_konv  TYPE tyt_konv.

  DATA:
    lt_aux_ekpo  TYPE tyt_ekpo.

  SELECT ebeln bukrs bsart ernam lifnr knumv
    FROM ekko
    INTO TABLE ch_t_ekko
  WHERE ebeln IN s_ebeln
    AND bukrs IN s_bukrs
    AND bsart IN s_bsart
    AND bedat IN s_bedat.

  IF ch_t_ekko[] IS NOT INITIAL.

    SELECT ebeln
      FROM ekpo
      INTO TABLE lt_aux_ekpo
      FOR ALL ENTRIES IN ch_t_ekko
    WHERE ebeln EQ ch_t_ekko-ebeln
      AND matnr IN s_matnr.

    IF lt_aux_ekpo[] IS NOT INITIAL.

      SELECT ebeln ebelp werks lgort matnr meins txz01
        FROM ekpo
        INTO TABLE ch_t_ekpo
        FOR ALL ENTRIES IN lt_aux_ekpo
      WHERE ebeln EQ lt_aux_ekpo-ebeln.

    ENDIF.

    SELECT bsart bstyp spras batxt
      FROM t161t
      INTO TABLE ch_t_t161t
      FOR ALL ENTRIES IN ch_t_ekko
    WHERE bsart EQ ch_t_ekko-bsart
      AND spras EQ sy-langu.

    SELECT lifnr name1
      FROM lfa1
      INTO TABLE ch_t_lfa1
      FOR ALL ENTRIES IN ch_t_ekko
    WHERE lifnr EQ ch_t_ekko-lifnr.

    SELECT knumv kposn stunr zaehk kbetr kschl
      FROM konv
      INTO TABLE ch_t_konv
      FOR ALL ENTRIES IN ch_t_ekko
    WHERE knumv EQ ch_t_ekko-knumv
      AND kschl EQ 'PBXX'.

    IF ch_t_ekpo[] IS NOT INITIAL.

      SELECT ebeln ebelp etenr eindt menge
        FROM eket
        INTO TABLE ch_t_eket
        FOR ALL ENTRIES IN ch_t_ekpo
      WHERE ebeln EQ ch_t_ekpo-ebeln
        AND ebelp EQ ch_t_ekpo-ebelp.

      SELECT matnr spras maktx
        FROM makt
        INTO TABLE ch_t_makt
         FOR ALL ENTRIES IN ch_t_ekpo
      WHERE matnr EQ ch_t_ekpo-matnr
        AND spras EQ sy-langu.

    ENDIF.

  ENDIF.

ENDFORM.                    " GET_DATA_DB

*&---------------------------------------------------------------------*
*&      Form  PROCESS_DATA
*&---------------------------------------------------------------------*
FORM process_data
  USING
    us_t_ekko  TYPE tyt_ekko
    us_t_t161t TYPE tyt_t161t
    us_t_lfa1  TYPE tyt_lfa1
    us_t_ekpo  TYPE tyt_ekpo
    us_t_eket  TYPE tyt_eket
    us_t_makt  TYPE tyt_makt
    us_t_konv  TYPE tyt_konv
  CHANGING
    ch_t_alv   TYPE tyt_alv.

  FIELD-SYMBOLS:
    <lfs_ekko>  TYPE ty_ekko,
    <lfs_t161t> TYPE ty_t161t,
    <lfs_lfa1>  TYPE ty_lfa1,
    <lfs_ekpo>  TYPE ty_ekpo,
    <lfs_eket>  TYPE ty_eket,
    <lfs_makt>  TYPE ty_makt,
    <lfs_konv>  TYPE ty_konv,
    <lfs_alv>   TYPE ty_alv.

  LOOP AT us_t_ekko ASSIGNING <lfs_ekko>.

    READ TABLE us_t_t161t ASSIGNING <lfs_t161t>
      WITH KEY bsart = <lfs_ekko>-bsart
               spras = sy-langu.

    READ TABLE us_t_lfa1 ASSIGNING <lfs_lfa1>
      WITH KEY lifnr = <lfs_ekko>-lifnr.

    LOOP AT us_t_ekpo ASSIGNING <lfs_ekpo>
      WHERE ebeln EQ <lfs_ekko>-ebeln.

      READ TABLE us_t_konv ASSIGNING <lfs_konv>
        WITH KEY knumv = <lfs_ekko>-knumv
                 kposn = <lfs_ekpo>-ebelp.

      LOOP AT us_t_eket ASSIGNING <lfs_eket>
        WHERE ebeln EQ <lfs_ekpo>-ebeln
          AND ebelp EQ <lfs_ekpo>-ebelp.

        APPEND INITIAL LINE TO ch_t_alv ASSIGNING <lfs_alv>.

        IF <lfs_alv> IS ASSIGNED.

          IF <lfs_ekko> IS ASSIGNED.

            <lfs_alv>-ebeln = <lfs_ekko>-ebeln.
            <lfs_alv>-bukrs = <lfs_ekko>-bukrs.
            <lfs_alv>-bsart = <lfs_ekko>-bsart.
            <lfs_alv>-ernam = <lfs_ekko>-ernam.
            <lfs_alv>-lifnr = <lfs_ekko>-lifnr.

            IF <lfs_t161t> IS ASSIGNED.

              <lfs_alv>-bsart_desc = <lfs_t161t>-batxt.

            ENDIF.

            IF <lfs_lfa1> IS ASSIGNED.

              <lfs_alv>-lifnr_name = <lfs_lfa1>-name1.

            ENDIF.

            IF <lfs_ekpo> IS ASSIGNED.

              <lfs_alv>-ebelp = <lfs_ekpo>-ebelp.
              <lfs_alv>-werks = <lfs_ekpo>-werks.
              <lfs_alv>-lgort = <lfs_ekpo>-lgort.
              <lfs_alv>-meins = <lfs_ekpo>-meins.

              IF <lfs_eket> IS ASSIGNED.

                <lfs_alv>-etenr = <lfs_eket>-etenr.
                <lfs_alv>-eindt = <lfs_eket>-eindt.
                <lfs_alv>-menge = <lfs_eket>-menge.

              ENDIF.

              IF <lfs_ekpo>-matnr IS NOT INITIAL.

                READ TABLE us_t_makt ASSIGNING <lfs_makt>
                  WITH KEY matnr = <lfs_ekpo>-matnr.

                IF <lfs_makt> IS ASSIGNED.

                  <lfs_alv>-matnr      = <lfs_makt>-matnr.
                  <lfs_alv>-matnr_desc = <lfs_makt>-maktx.

                ENDIF.

              ELSE.

                <lfs_alv>-matnr_desc = <lfs_ekpo>-txz01.

              ENDIF.

            ENDIF.

            IF <lfs_konv> IS ASSIGNED.

              <lfs_alv>-kbetr       = <lfs_konv>-kbetr.
              <lfs_alv>-kbetr_total = <lfs_eket>-menge * <lfs_konv>-kbetr.

            ENDIF.

          ENDIF.

        ENDIF.

      ENDLOOP.

    ENDLOOP.

  ENDLOOP.

ENDFORM.                    " PROCESS_DATA

*&---------------------------------------------------------------------*
*&      Form  SHOW_REPORT
*&---------------------------------------------------------------------*
FORM show_report
  USING us_t_alv TYPE tyt_alv.

  DATA:
    le_layout           TYPE slis_layout_alv,
    lt_fieldcat         TYPE slis_t_fieldcat_alv,
    lv_callback_program TYPE sy-repid,
    lt_sort             TYPE slis_t_sortinfo_alv.

  lv_callback_program = sy-repid.

  PERFORM set_layout
    CHANGING
      le_layout.

  PERFORM set_fieldcat
    CHANGING
      lt_fieldcat[].

  PERFORM set_sort
    CHANGING
      lt_sort[].

  SORT us_t_alv.

  CALL FUNCTION 'REUSE_ALV_GRID_DISPLAY'
    EXPORTING
      i_callback_program       = lv_callback_program
      is_layout                = le_layout
      it_fieldcat              = lt_fieldcat
      i_callback_pf_status_set = 'SET_STATUS_ALV'
      i_callback_user_command  = 'ALV_USER_COMMAND'
      it_sort                  = lt_sort
    TABLES
      t_outtab                 = us_t_alv
    EXCEPTIONS
      program_error            = 1
      OTHERS                   = 2.

  IF sy-subrc <> 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
            WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.

ENDFORM.                    " SHOW_REPORT