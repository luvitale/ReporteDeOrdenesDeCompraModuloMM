*&---------------------------------------------------------------------*
*&  Include           ZAALV_REP_ORD_COMP_MM_MOV
*&---------------------------------------------------------------------*

INCLUDE:
  zaalv_rep_ord_comp_mm_mov_top,
  zaalv_rep_ord_comp_mm_mov_f01.

*&---------------------------------------------------------------------*
*&      Form  SHOW_MOVEMENTS
*&---------------------------------------------------------------------*
FORM show_movements
  USING
    us_t_alv TYPE tyt_alv.

  CLEAR t_alv_movements.

  PERFORM get_movements_data
    USING
      us_t_alv[]
    CHANGING
      t_alv_movements[].

  IF t_alv_movements[] IS NOT INITIAL.

    PERFORM show_movements_report
      USING
        t_alv_movements[].

  ELSE.

    MESSAGE 'No hay movimientos para la orden de compra seleccionada'(e02) TYPE 'S' DISPLAY LIKE 'E'.

  ENDIF.

ENDFORM.                    " SHOW_MOVEMENTS