*&---------------------------------------------------------------------*
*&  Include           ZAALV_REP_ORD_COMP_MM_TOP
*&---------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*& TYPE-POOLS
*&---------------------------------------------------------------------*
TYPE-POOLS:
  slis,
  abap.

*&---------------------------------------------------------------------*
*& TABLES
*&---------------------------------------------------------------------*
TABLES:
  ekko,
  ekpo.

*&---------------------------------------------------------------------*
*& TYPES
*&---------------------------------------------------------------------*
TYPES:
  BEGIN OF ty_ekko,
    ebeln TYPE ekko-ebeln,
    bukrs TYPE ekko-bukrs,
    bsart TYPE ekko-bsart,
    ernam TYPE ekko-ernam,
    lifnr TYPE ekko-lifnr,
    knumv TYPE ekko-knumv,
  END OF ty_ekko,

  tyt_ekko TYPE STANDARD TABLE OF ty_ekko.

TYPES:
 BEGIN OF ty_ekpo,
   ebeln TYPE ekpo-ebeln,
   ebelp TYPE ekpo-ebelp,
   werks TYPE ekpo-werks,
   lgort TYPE ekpo-lgort,
   matnr TYPE ekpo-matnr,
   meins TYPE ekpo-meins,
   txz01 TYPE ekpo-txz01,
 END OF ty_ekpo,

tyt_ekpo TYPE STANDARD TABLE OF ty_ekpo.

TYPES:
 BEGIN OF ty_t161t,
   bsart TYPE t161t-bsart,
   bstyp TYPE t161t-bstyp,
   spras TYPE t161t-spras,
   batxt TYPE t161t-batxt,
 END OF ty_t161t,

tyt_t161t TYPE STANDARD TABLE OF ty_t161t.

TYPES:
 BEGIN OF ty_lfa1,
   lifnr TYPE lfa1-lifnr,
   name1 TYPE lfa1-name1,
 END OF ty_lfa1,

tyt_lfa1 TYPE STANDARD TABLE OF ty_lfa1.


TYPES:
 BEGIN OF ty_makt,
   matnr TYPE makt-matnr,
   spras TYPE makt-spras,
   maktx TYPE makt-maktx,
 END OF ty_makt,

tyt_makt TYPE STANDARD TABLE OF ty_makt.

TYPES:
 BEGIN OF ty_konv,
   knumv TYPE konv-knumv,
   kposn TYPE konv-kposn,
   stunr TYPE konv-stunr,
   zaehk TYPE konv-zaehk,
   kbetr TYPE konv-kbetr,
   ksch1 TYPE konv-kschl,
 END OF ty_konv,

tyt_konv TYPE STANDARD TABLE OF ty_konv.

TYPES:
 BEGIN OF ty_eket,
   ebeln TYPE eket-ebeln,
   ebelp TYPE eket-ebelp,
   etenr TYPE eket-etenr,
   eindt TYPE eket-eindt,
   menge TYPE eket-menge,
 END OF ty_eket,

tyt_eket TYPE STANDARD TABLE OF ty_eket.

TYPES:
  BEGIN OF ty_alv,
    sel TYPE char1,
    ebeln TYPE ekko-ebeln,
    ebelp TYPE ekpo-ebelp,
    etenr TYPE eket-etenr,
    eindt TYPE eket-eindt,
    bukrs TYPE ekko-bukrs,
    bsart TYPE ekko-bsart,
    bsart_desc TYPE t161t-batxt,
    ernam TYPE ekko-ernam,
    lifnr TYPE ekko-lifnr,
    lifnr_name TYPE lfa1-name1,
    werks TYPE ekpo-werks,
    lgort TYPE ekpo-lgort,
    matnr TYPE ekpo-matnr,
    matnr_desc TYPE makt-maktx,
    menge TYPE eket-menge,
    meins TYPE ekpo-meins,
    kbetr TYPE konv-kbetr,
    kbetr_total TYPE konv-kbetr,
  END OF ty_alv,

  tyt_alv TYPE STANDARD TABLE OF ty_alv.

DATA:
  t_alv TYPE tyt_alv.