# Reporte de Órdenes de Compra – Módulo MM

Reporte ALV de Órdenes de Compra (Tabla EKKO), sus Posiciones (Tabla EKPO) y Repartos (Tabla EKET), con las siguientes características:

## Filtros de Pantalla

* Orden de Compra (EKKO-EBELN)
* Sociedad (EKKO-BUKRS)
* Clase de Documento (EKKO-BSART)
* Fecha del Documento (EKKO-BEDAT)
* Material (EKPO-MATNR)¹

¹*Si la Orden de Compra posee el material en algunas de sus posiciones, se desea ver todas las posiciones.*

Por ejemplo, la Orden de Compra 4500000004 tiene dos posiciones. En una, el material H100, y, en la otra, el H200.

Si en la Pantalla de Selección se ingresa el material H100, en el Reporte se debería visualizar tanto la posición 10 como la 20, la Orden de Compra completa, ya que posee al menos un material que cumple el filtro de pantalla.

### Relación de las tablas EKKO/EKPO/EKET

#### EKKO > EKPO (1 a Muchos)

```abap
EKPO-EBELN = EKKO-EBELN
```

#### EKPO > EKET (1 a Muchos)

```abap
EKET-EBELN = EKPO-EBELN
EKET-EBELP = EKPO-EBELP
```

## Reporte

* Orden de Compra (EKKO-EBELN)
* Posición (EKPO-EBELP)
* Reparto (EKET-ETENR)
* Fecha del Reparto (EKET-EINDT)
* Sociedad (EKKO-BUKRS)
* Clase de Doc. (EKKO-BSART)
* Descripción (de la Clase de Doc –buscar la tabla con el elemento dato/dominio- )
* Usuario (EKKO-ERNAM)
* Proveedor (EKKO-LIFNR)
* Nombre (LFA1-NAME1, ingresando con LFA1-LIFNR = EKKO-LIFNR)
* Centro (EKPO-WERKS)
* Almacén (EKPO-LGORT)
* Material (EKPO-MATNR)
* Descripción (Sí EKPO-MATNR no es vacío, mostrar MAKT-MAKTX, sino mostrar EKPO-TXZ01)
* Cantidad (EKET-MENGE)
* Unidad (EKPO-MEINS)
* Precio Unitario (KONV-KBETR, KSCHL = ‘PBXX’ – ingresando con KONV-KNUMV = EKKO-KNUMV y KONV-KPOSN = EKPO-EBELP)
* Importe Total (Cantidad * Precio Unitario)

El Reporte debe estar ordenado por “Orden de Compra” y subtotalizado/totalizado por “Importe Total”

## Hotspot

### Campo “Orden de Compra”

```abap
SET PARAMETER ID 'BES' FIELD EBELN.
CALL TRANSACTION 'ME23N'.
```

### Campo “Proveedor”

Se desea llamar a la Trx XK03 (Averiguar el Parameter ID)

### Campo “Material”

Se desea llamar a la Trx MM03 (Averiguar el Parameter ID)

## Botones adicionales

Agregar un botón (“Movimientos”) que muestre un Segundo ALV de los Movimientos (Tabla MKPF/MSEG) de las líneas seleccionadas.

Mostrar:

* Orden de Compra (EKKO-EBELN -Seleccionado en el ALV anterior-)
* Posición de Compra (EKKO-EBELP -Seleccionado en el ALV anterior-)
* Movimiento (MSEG-MBLNR)
* Año (MSEG-MJAHR)
* Posición (MSEG-ZEILE)
* Clase (MKPF-BLART)
* Fecha de Documento (MKPF-BLDAT)
* Fecha de Contabilización (MKPF-BUDAT)
* Clase de Movimiento (MSEG-BWART)
* Cantidad (MSEG-MENGE)
* Unidad (MSEG-MEINS)
* Importe (MSEG-DMBTR)
* Moneda (MSEG-WAERS)

### Tabla EKPO > Tabla MSEG (1 a Muchos)

```abap
MSEG-EBELN = EKPO-EBELN " este se muestra en el 1er ALV
MSEG-EBELP = EKPO-EBELP " este se muestra en el 1er ALV
```

### Tabla MSEG > Tabla MKPF (Muchos a 1)

```abap
MKPF-MBLNR = MSEG-MBLNR
MKPF-MJAHR = MSEG-MJAHR
```
