*&---------------------------------------------------------------------*
*&  Include           ZAALV_REP_ORD_COMP_MM_SCR
*&---------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*& SELECTION-SCREEN
*&---------------------------------------------------------------------*
SELECTION-SCREEN BEGIN OF BLOCK b01 WITH FRAME TITLE text-b01.

SELECT-OPTIONS:
  s_ebeln FOR ekko-ebeln,
  s_bukrs FOR ekko-bukrs,
  s_bsart FOR ekko-bsart,
  s_bedat FOR ekko-bedat,
  s_matnr FOR ekpo-matnr.

SELECTION-SCREEN END OF BLOCK b01.