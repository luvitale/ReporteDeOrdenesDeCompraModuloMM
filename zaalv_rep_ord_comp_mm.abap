*&---------------------------------------------------------------------*
*& Report  ZAALV_REP_ORD_COMP_MM
*&
*&---------------------------------------------------------------------*
*&
*&
*&---------------------------------------------------------------------*

REPORT  zaalv_rep_ord_comp_mm.

*&---------------------------------------------------------------------*
*& INCLUDE
*&---------------------------------------------------------------------*
INCLUDE:
  zaalv_rep_ord_comp_mm_top,
  zaalv_rep_ord_comp_mm_scr,
  zaalv_rep_ord_comp_mm_f01.

*&---------------------------------------------------------------------*
*& END-OF-SELECTION
*&---------------------------------------------------------------------*
END-OF-SELECTION.

  PERFORM show_purchase_orders
    USING
      t_alv[].

*&---------------------------------------------------------------------*
*&      Form  SHOW_PURCHASE_ORDERS
*&---------------------------------------------------------------------*
FORM show_purchase_orders
  USING
    us_t_alv TYPE tyt_alv.

  PERFORM get_data
    CHANGING
      t_alv[].

  IF t_alv[] IS NOT INITIAL.

    PERFORM show_report
      USING
        t_alv[].

  ELSE.

    MESSAGE 'No hay datos para los par�metros de selecci�n'(e01) TYPE 'S' DISPLAY LIKE 'E'.

  ENDIF.

ENDFORM.                    " SHOW_PURCHASE_ORDERS