*&---------------------------------------------------------------------*
*&  Include           ZAALV_REP_ORD_COMP_MM_MOV_F02
*&---------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*&      Form  SET_MOVEMENTS_LAYOUT
*&---------------------------------------------------------------------*
FORM set_movements_layout
  CHANGING
    ch_e_layout TYPE slis_layout_alv.

  ch_e_layout-colwidth_optimize = abap_true.
  ch_e_layout-zebra = abap_true.

ENDFORM.                    " SET_MOVEMENTS_LAYOUT

*&---------------------------------------------------------------------*
*&      Form  SET_MOVEMENTS_FIELDCAT
*&---------------------------------------------------------------------*
FORM set_movements_fieldcat
  CHANGING
    ch_t_fieldcat TYPE slis_t_fieldcat_alv.

  PERFORM set_movements_fieldcat_field
    USING
      'T_ALV_MOVEMENTS'
      'EBELN'
      'Orden de Compra'(f19)
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_movements_fieldcat_field
    USING
      'T_ALV_MOVEMENTS'
      'EBELP'
      'Posici�n de Compra'(f20)
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_movements_fieldcat_field
    USING
      'T_ALV_MOVEMENTS'
      'MBLNR'
      'Movimiento'(f21)
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_movements_fieldcat_field
    USING
      'T_ALV_MOVEMENTS'
      'MJAHR'
      'A�o'(f22)
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_movements_fieldcat_field
    USING
      'T_ALV_MOVEMENTS'
      'ZEILE'
      'Posici�n'(f23)
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_movements_fieldcat_field
    USING
      'T_ALV_MOVEMENTS'
      'BLART'
      'Clase'(f24)
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_movements_fieldcat_field
    USING
      'T_ALV_MOVEMENTS'
      'BLDAT'
      'Fecha de Documento'(f25)
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_movements_fieldcat_field
    USING
      'T_ALV_MOVEMENTS'
      'BUDAT'
      'Fecha de Contabilizaci�n'(f26)
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_movements_fieldcat_field
    USING
      'T_ALV_MOVEMENTS'
      'BWART'
      'Clase de Movimiento'(f27)
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_movements_fieldcat_field
    USING
      'T_ALV_MOVEMENTS'
      'MENGE'
      'Cantidad'(f28)
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_movements_fieldcat_field
    USING
      'T_ALV_MOVEMENTS'
      'MEINS'
      'Unidad'(f29)
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_movements_fieldcat_field
    USING
      'T_ALV_MOVEMENTS'
      'DMBTR'
      'Importe'(f30)
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_movements_fieldcat_field
    USING
      'T_ALV_MOVEMEMENTS'
      'WAERS'
      'Moneda'(f31)
    CHANGING
      ch_t_fieldcat[].

ENDFORM.                    " SET_MOVEMENTS_FIELDCAT


*&---------------------------------------------------------------------*
*&      Form  SET_MOVEMENTS_FIELDCAT_FIELD
*&---------------------------------------------------------------------*
FORM set_movements_fieldcat_field
  USING
    us_v_tabname
    us_v_field_name
    us_v_text
  CHANGING
    ch_t_fieldcat TYPE slis_t_fieldcat_alv.

  DATA:
    le_fieldcat TYPE LINE OF slis_t_fieldcat_alv.

  CLEAR le_fieldcat.

  le_fieldcat-tabname   = us_v_tabname.
  le_fieldcat-fieldname = us_v_field_name.
  le_fieldcat-seltext_l = us_v_text.

  APPEND le_fieldcat TO ch_t_fieldcat.

ENDFORM.                    " SET_MOVEMENTS_FIELDCAT_FIELD