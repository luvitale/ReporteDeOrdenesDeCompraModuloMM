*&---------------------------------------------------------------------*
*&  Include           ZAALV_REP_ORD_COMP_MM_MOV_TOP
*&---------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*& TYPES
*&---------------------------------------------------------------------*
TYPES:
  BEGIN OF ty_mseg,
    mblnr TYPE mseg-mblnr,
    mjahr TYPE mseg-mjahr,
    zeile TYPE mseg-zeile,
    ebeln TYPE mseg-ebeln,
    ebelp TYPE mseg-ebelp,
    bwart TYPE mseg-bwart,
    menge TYPE mseg-menge,
    meins TYPE mseg-meins,
    dmbtr TYPE mseg-dmbtr,
    waers TYPE mseg-waers,
  END OF ty_mseg,

  tyt_mseg TYPE STANDARD TABLE OF ty_mseg.

TYPES:
  BEGIN OF ty_mkpf,
    mblnr TYPE mkpf-mblnr,
    mjahr TYPE mkpf-mjahr,
    blart TYPE mkpf-blart,
    bldat TYPE mkpf-bldat,
    budat TYPE mkpf-budat,
  END OF ty_mkpf,

  tyt_mkpf TYPE STANDARD TABLE OF ty_mkpf.

TYPES:
  BEGIN OF ty_alv_movements,
    ebeln TYPE ekpo-ebeln,
    ebelp TYPE ekpo-ebelp,
    mblnr TYPE mseg-mblnr,
    mjahr TYPE mseg-mjahr,
    zeile TYPE mseg-zeile,
    blart TYPE mkpf-blart,
    bldat TYPE mkpf-bldat,
    budat TYPE mkpf-budat,
    bwart TYPE mseg-bwart,
    menge TYPE mseg-menge,
    meins TYPE mseg-meins,
    dmbtr TYPE mseg-dmbtr,
    waers TYPE mseg-waers,
  END OF ty_alv_movements,

  tyt_alv_movements TYPE STANDARD TABLE OF ty_alv_movements.

*&---------------------------------------------------------------------*
*& DATA
*&---------------------------------------------------------------------*
DATA:
  t_alv_movements TYPE tyt_alv_movements.